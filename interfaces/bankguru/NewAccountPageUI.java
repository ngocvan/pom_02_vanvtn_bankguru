package bankguru;

public class NewAccountPageUI {
	public static final String CUSTOMERID_TXT = "//input[@name='cusid']";
	public static final String ACCOUNT_TYPE_DROPDOWN = "//select[@name='selaccount']";
	public static final String INITIAL_DEPOSIT_TXT = "//input[@name='inideposit']";	
	public static final String SUBMIT_BTN = "//input[@name='button2']";
	public static final String SUCCESSFUL_MSG= "//table[@id='account']//p[@class='heading3']";
	public static final String ACCOUNT_ID= "//td[contains(text(),'Account ID')]/following-sibling::td";
	public static final String CURRENT_AMOUNT= "//td[contains(text(),'Current Amount')]/following-sibling::td";
}
