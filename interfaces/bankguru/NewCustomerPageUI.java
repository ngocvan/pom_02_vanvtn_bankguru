package bankguru;

public class NewCustomerPageUI {
	public static final String CUSTOMER_NAME_TXT = "//input[@name='name']";
	public static final String DATE_OF_BIRTH_TXT = "//*[@id='dob']";
	public static final String ADDRESS_TXT = "//td[text()='Address']/following-sibling::td/textarea";
	public static final String CITY_TXT = "//input[@name='city']";
	public static final String STATE_TXT = "//input[@name='state']";
	public static final String PIN_TXT = "//input[@name='pinno']";
	public static final String MOBILE_NUMBER_TXT = "//input[@name='telephoneno']";
	public static final String EMAIL_TXT = "//input[@name='emailid']";
	public static final String PASSWORD_TXT = "//input[@name='password']";
	public static final String SUBMIT_BTN = "//input[@value='Submit']";
	public static final String SUCCESSFUL_MSG= "//table[@id='customer']//p[@class='heading3']";
	public static final String CUSTOMERID= "//td[contains(text(),'Customer ID')]/following-sibling::td";
	public static final String ERROR_LABEL = "//label[contains(@id,'message')][@style='visibility: visible;']";
}
