package commons;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bankguru.AbstractPageUI;
import pages.BalanceEnquiryPage;
import pages.DeleteAcountPage;
import pages.DeleteCustomerPage;
import pages.EditCustomerPage;
import pages.FundTransferPage;
import pages.NewAccountPage;
import pages.NewCustomerPage;
import pages.TransactionPage;

public class AbstractPage {

	public void openURL(WebDriver driver, String url) {
		driver.get(url);
	}

	public String getTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String getCurrentURL(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public void back(WebDriver driver) {
		driver.navigate().back();
	}

	public void forward(WebDriver driver) {
		driver.navigate().forward();
	}

	public void refresh(WebDriver driver) {
		driver.navigate().refresh();
	}

	// Web Element
	public void clickToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clickToElement(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void sendkeyToElement(WebDriver driver, String locator, String text) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (element.isEnabled() && !(getAttributeValue(driver, locator, "type").equalsIgnoreCase("date"))) {
			element.clear();
			element.sendKeys(text);
		} else
			element.sendKeys(text);
	}

	public void selectItemInDropdown(WebDriver driver, String locator, String item) {
		Select select = new Select(driver.findElement(By.xpath(locator)));
		select.selectByVisibleText(item);
	}

	public String getFirstItemSelect(WebDriver driver, String locator) {
		Select select = new Select(driver.findElement(By.xpath(locator)));
		return select.getFirstSelectedOption().getText();
	}

	public String getAttributeValue(WebDriver driver, String locator, String attributeName) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attributeName);
	}

	public String getTextElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getText().trim();
	}

	public int getSize(WebDriver driver, String locator) {
		List<WebElement> list = driver.findElements(By.xpath(locator));
		return list.size();
	}

	public void uncheckTheCheckbox(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (element.isSelected()) {
			element.click();
		}
	}

	public boolean isControlDisplay(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isDisplayed();
	}

	public boolean isControlDisplay(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isDisplayed();
	}

	public boolean isControlSelect(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isSelected();
	}

	public boolean isControlEnable(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isEnabled();
	}

	public void acceptAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void cancelAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public String getTextAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		return alert.getText();
	}

	public void sendkeyToAlert(WebDriver driver, String text) {
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(text);
	}

	public void switchToWindowByID(WebDriver driver, String parent) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindow : allWindows) {
			if (!runWindow.equals(parent)) {
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public void switchToWindowByTitle(WebDriver driver, String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			driver.switchTo().window(runWindows);
			String currentWin = driver.getTitle();
			if (currentWin.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(WebDriver driver, String parentWindow) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			if (!runWindows.equals(parentWindow)) {
				driver.switchTo().window(runWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parentWindow);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	public void switchToIframe(WebDriver driver, String locator) {
		WebElement iframe = driver.findElement(By.xpath(locator));
		driver.switchTo().frame(iframe);
	}

	public void doubleClick(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.doubleClick(element).perform();
	}

	public void hoverMouse(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();
	}

	public void rightClick(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.contextClick(element).perform();
	}

	public void dragAndDrop(WebDriver driver, String sourceLocator, String targetLocator) {
		WebElement source = driver.findElement(By.xpath(sourceLocator));
		WebElement target = driver.findElement(By.xpath(targetLocator));
		Actions action = new Actions(driver);
		action.dragAndDrop(source, target).release().perform();
	}

	public void keyUp(WebDriver driver, String locator, Keys modifier_key) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.keyUp(element, modifier_key).perform();
	}

	public void dynamicKeyPress(WebDriver driver, String locator, Keys modifier_key) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.sendKeys(element, modifier_key).perform();
	}

	public void uploadBySenkey(WebDriver driver, String locator, String filePath) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(filePath);
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForWebElementByClick(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebDriver driver, WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToElement(WebDriver driver, String locator) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(By.xpath(locator));
			return js.executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public void waitForElementPresence(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public void waitForElementVisible(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void waitForElementVisible(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	public void waitForElementNotVisible(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	public void waitForElementClickable(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	public void waitForAlertPresence(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.alertIsPresent());
	}

	public NewCustomerPage openNewCustomerPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "New Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "New Customer");
		return PageFactoryManager.getNewCustomerPage(driver);
	}

	public EditCustomerPage openEditCustomerPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Edit Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Edit Customer");
		return PageFactoryManager.getEditCustomerPage(driver);
	}

	public DeleteCustomerPage openDeleteCustomerPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Delete Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Delete Customer");
		return PageFactoryManager.getDeleteCustomerPage(driver);
	}

	public NewAccountPage openNewAccountPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "New Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "New Account");
		return PageFactoryManager.getNewAccountPage(driver);
	}
	
	public TransactionPage openDepositPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Deposit");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Deposit");
		return PageFactoryManager.getTransactionPage(driver);
	}
	
	public TransactionPage openWithdrawalPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Withdrawal");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Withdrawal");
		return PageFactoryManager.getTransactionPage(driver);
	}
	
	public FundTransferPage openFundTransferPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Fund Transfer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Fund Transfer");
		return PageFactoryManager.getFundTransferPage(driver);
	}
	
	public BalanceEnquiryPage openBalanceEnquiryPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Balance Enquiry");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Balance Enquiry");
		return PageFactoryManager.getBalanceEnquiryPage(driver);
	}
	
	public DeleteAcountPage openDeleteAcountPage(WebDriver driver) {
		waitForElementVisible(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Delete Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_MENU_PAGE, "Delete Account");
		return PageFactoryManager.getDeleteAcountPage(driver);
	}
	
}
