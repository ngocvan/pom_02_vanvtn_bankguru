package commons;

import org.openqa.selenium.WebDriver;

import pages.BalanceEnquiryPage;
import pages.DeleteAcountPage;
import pages.DeleteCustomerPage;
import pages.EditCustomerPage;
import pages.FundTransferPage;
import pages.HomePage;
import pages.LoginPage;
import pages.NewAccountPage;
import pages.NewCustomerPage;
import pages.RegisterPage;
import pages.TransactionPage;

public class PageFactoryManager {
	private static HomePage homePage;
	private static RegisterPage registerPage;
	private static LoginPage loginPage;
	private static NewCustomerPage newCustomerPage;
	private static EditCustomerPage editCustomerPage;
	private static DeleteCustomerPage deleteCustomerPage;
	private static NewAccountPage newAccountPage;
	private static TransactionPage transactionPage;
	private static FundTransferPage fundTransferPage;
	private static BalanceEnquiryPage balanceEnquiryPage;
	private static DeleteAcountPage deleteAcountPage;

	public static LoginPage getLoginPage(WebDriver driver) {
		if (loginPage == null) {
			return new LoginPage(driver);
		}
		return loginPage;
	}

	public static RegisterPage getRegisterPage(WebDriver driver) {
		if (registerPage == null) {
			return new RegisterPage(driver);
		}
		return registerPage;
	}

	public static HomePage getHomePage(WebDriver driver) {
		if (homePage == null) {
			return new HomePage(driver);
		}
		return homePage;
	}

	public static NewCustomerPage getNewCustomerPage(WebDriver driver) {
		if (newCustomerPage == null) {
			return new NewCustomerPage(driver);
		}
		return newCustomerPage;
	}

	public static EditCustomerPage getEditCustomerPage(WebDriver driver) {
		if (editCustomerPage == null) {
			return new EditCustomerPage(driver);
		}
		return editCustomerPage;
	}

	public static DeleteCustomerPage getDeleteCustomerPage(WebDriver driver) {
		if (deleteCustomerPage == null) {
			return new DeleteCustomerPage(driver);
		}
		return deleteCustomerPage;
	}

	public static NewAccountPage getNewAccountPage(WebDriver driver) {
		if (newAccountPage == null) {
			return new NewAccountPage(driver);
		}
		return newAccountPage;
	}

	public static TransactionPage getTransactionPage(WebDriver driver) {
		if (transactionPage == null) {
			return new TransactionPage(driver);
		}
		return transactionPage;
	}

	public static FundTransferPage getFundTransferPage(WebDriver driver) {
		if (fundTransferPage == null) {
			return new FundTransferPage(driver);
		}
		return fundTransferPage;
	}

	public static BalanceEnquiryPage getBalanceEnquiryPage(WebDriver driver) {
		if (balanceEnquiryPage == null) {
			return new BalanceEnquiryPage(driver);
		}
		return balanceEnquiryPage;
	}

	public static DeleteAcountPage getDeleteAcountPage(WebDriver driver) {
		if (deleteAcountPage == null) {
			return new DeleteAcountPage(driver);
		}
		return deleteAcountPage;
	}

}
