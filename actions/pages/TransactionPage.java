package pages;

import org.openqa.selenium.WebDriver;

import bankguru.TransactionPageUI;
import commons.AbstractPage;

public class TransactionPage extends AbstractPage {
	WebDriver driver;

	public TransactionPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputAcountNo(String accountNo) {
		waitForElementVisible(driver, TransactionPageUI.ACCOUNT_NO_TXT);
		sendkeyToElement(driver, TransactionPageUI.ACCOUNT_NO_TXT, accountNo);
	}

	public void inputAmount(String amount) {
		sendkeyToElement(driver, TransactionPageUI.AMOUNT_TXT, amount);
	}
	
	public void inputDescription(String description) {
		sendkeyToElement(driver, TransactionPageUI.DESCRIPTION_TXT, description);
	}
	
	public void clickSubmitButton() {
		clickToElement(driver, TransactionPageUI.SUBMIT_BTN);
	}

	public String getDisplayMessage() {
		waitForElementVisible(driver, TransactionPageUI.DISPLAY_MSG);
		return getTextElement(driver, TransactionPageUI.DISPLAY_MSG).trim();
	}

	public String getCurrentBalance() {
		waitForElementVisible(driver, TransactionPageUI.CURRENT_BALANCE);
		return getTextElement(driver, TransactionPageUI.CURRENT_BALANCE).trim();
	}	
	

}
