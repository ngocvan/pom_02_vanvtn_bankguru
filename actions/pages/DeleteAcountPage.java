package pages;

import org.openqa.selenium.WebDriver;

import bankguru.DeleteAcountPageUI;
import commons.AbstractPage;

public class DeleteAcountPage extends AbstractPage {
	WebDriver driver;

	public DeleteAcountPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputAcountNo(String accountNo) {
		waitForElementVisible(driver, DeleteAcountPageUI.ACCOUNT_NO_TXT);
		sendkeyToElement(driver, DeleteAcountPageUI.ACCOUNT_NO_TXT, accountNo);
	}
	
	public void clickSubmitButton() {
		clickToElement(driver, DeleteAcountPageUI.SUBMIT_BTN);
	}

	public void clickOKButtonOnAlert() {
		waitForAlertPresence(driver);
		acceptAlert(driver);
	}
	
	public String getTextOnAlert() {
		waitForAlertPresence(driver);
		return getTextAlert(driver);
	}

}
