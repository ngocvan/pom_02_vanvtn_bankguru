package pages;

import org.openqa.selenium.WebDriver;

import bankguru.HomePageUI;
import commons.AbstractPage;

public class HomePage extends AbstractPage {
	WebDriver driver;

	public HomePage(WebDriver _driver) {
		this.driver = _driver;
	}

	public String getWelcomeHomepageMsg() {
		waitForElementVisible(driver, HomePageUI.WELLCOME_HOMEPAGE_MSG);
		return getTextElement(driver, HomePageUI.WELLCOME_HOMEPAGE_MSG);
	}

}
