package pages;

import org.openqa.selenium.WebDriver;

import bankguru.RegisterPageUI;
import commons.AbstractPage;
import commons.PageFactoryManager;

public class RegisterPage extends AbstractPage {
	WebDriver driver;

	public RegisterPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputEmail(String email) {
		waitForElementVisible(driver, RegisterPageUI.EMAIL_TXT);
		sendkeyToElement(driver, RegisterPageUI.EMAIL_TXT, email);
	}

	public void clickSubmitButton() {
		clickToElement(driver, RegisterPageUI.SUBMIT_BTN);
	}

	public String getUsername() {
		waitForElementVisible(driver, RegisterPageUI.USERNAME_TEXT);
		return getTextElement(driver, RegisterPageUI.USERNAME_TEXT);
	}

	public String getPassword() {
		return getTextElement(driver, RegisterPageUI.PASSWORD_TEXT);
	}

	public LoginPage openLoginPage(String loginURL) {
		openURL(driver, loginURL);
		return PageFactoryManager.getLoginPage(driver);
	}
}
