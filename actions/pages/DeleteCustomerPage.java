package pages;

import org.openqa.selenium.WebDriver;

import bankguru.DeleteCustomerPageUI;
import commons.AbstractPage;

public class DeleteCustomerPage extends AbstractPage {
	WebDriver driver;

	public DeleteCustomerPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputCustomerID(String accountNo) {
		waitForElementVisible(driver, DeleteCustomerPageUI.CUSTOMER_ID_TXT);
		sendkeyToElement(driver, DeleteCustomerPageUI.CUSTOMER_ID_TXT, accountNo);
	}

	public void clickSubmitButton() {
		clickToElement(driver, DeleteCustomerPageUI.SUBMIT_BTN);
	}

	public void clickOKButtonOnAlert() {
		waitForAlertPresence(driver);
		acceptAlert(driver);
	}

	public String getTextOnAlert() {
		waitForAlertPresence(driver);
		return getTextAlert(driver);
	}
}
