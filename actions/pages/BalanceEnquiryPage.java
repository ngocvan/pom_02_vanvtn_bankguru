package pages;

import org.openqa.selenium.WebDriver;

import bankguru.BalanceEnquiryPageUI;
import commons.AbstractPage;

public class BalanceEnquiryPage extends AbstractPage {
	WebDriver driver;

	public BalanceEnquiryPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputAcountNo(String accountNo) {
		waitForElementVisible(driver, BalanceEnquiryPageUI.ACCOUNT_NO_TXT);
		sendkeyToElement(driver, BalanceEnquiryPageUI.ACCOUNT_NO_TXT, accountNo);
	}
	
	public void clickSubmitButton() {
		clickToElement(driver, BalanceEnquiryPageUI.SUBMIT_BTN);
	}

	public String getDisplayMessage() {
		waitForElementVisible(driver, BalanceEnquiryPageUI.DISPLAY_MSG);
		return getTextElement(driver, BalanceEnquiryPageUI.DISPLAY_MSG).trim();
	}

	public String getCurrentBalance() {
		waitForElementVisible(driver, BalanceEnquiryPageUI.CURRENT_BALANCE);
		return getTextElement(driver, BalanceEnquiryPageUI.CURRENT_BALANCE).trim();
	}	
	

}
