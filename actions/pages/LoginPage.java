package pages;

import org.openqa.selenium.WebDriver;

import bankguru.LoginPageUI;
import commons.AbstractPage;
import commons.PageFactoryManager;

public class LoginPage extends AbstractPage {
	WebDriver driver;

	public LoginPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public String getURL() {
		return getCurrentURL(driver);
	}

	public RegisterPage clickHereLink() {
		scrollToBottomPage(driver);
		waitForElementVisible(driver, LoginPageUI.HERE_LINK);
		clickToElement(driver, LoginPageUI.HERE_LINK);
		return PageFactoryManager.getRegisterPage(driver);
	}

	public void inputUsername(String username) {
		waitForElementVisible(driver, LoginPageUI.USERNAME_TXT);
		sendkeyToElement(driver, LoginPageUI.USERNAME_TXT, username);
	}

	public void inputPassword(String password) {
		sendkeyToElement(driver, LoginPageUI.PASSWORD_TXT, password);
	}

	public HomePage clickLoginButton() {
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		return PageFactoryManager.getHomePage(driver);
	}
}
