package pages;

import org.openqa.selenium.WebDriver;

import bankguru.NewAccountPageUI;
import commons.AbstractPage;

public class NewAccountPage extends AbstractPage {
	WebDriver driver;

	public NewAccountPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputCustomerID(String customerID) {
		waitForElementVisible(driver, NewAccountPageUI.CUSTOMERID_TXT);
		sendkeyToElement(driver, NewAccountPageUI.CUSTOMERID_TXT, customerID);
	}

	public void selectAccountType(String accountType) {
		selectItemInDropdown(driver, NewAccountPageUI.ACCOUNT_TYPE_DROPDOWN, accountType);
	}

	public void inputInitialDeposit(String initialDeposit) {
		sendkeyToElement(driver, NewAccountPageUI.INITIAL_DEPOSIT_TXT, initialDeposit);
	}
	
	public void clickSubmitButton() {
		clickToElement(driver, NewAccountPageUI.SUBMIT_BTN);
	}

	public String getSuccessfulMessage() {
		waitForElementVisible(driver, NewAccountPageUI.SUCCESSFUL_MSG);
		return getTextElement(driver, NewAccountPageUI.SUCCESSFUL_MSG).trim();
	}

	public String getAccountID() {
		waitForElementVisible(driver, NewAccountPageUI.ACCOUNT_ID);
		return getTextElement(driver, NewAccountPageUI.ACCOUNT_ID).trim();
	}
	
	public String getCurrentAmount() {
		return getTextElement(driver, NewAccountPageUI.CURRENT_AMOUNT).trim();
	}

}
