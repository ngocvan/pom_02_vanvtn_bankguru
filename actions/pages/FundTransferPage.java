package pages;

import org.openqa.selenium.WebDriver;

import bankguru.FundTransferPageUI;
import commons.AbstractPage;

public class FundTransferPage extends AbstractPage {
	WebDriver driver;

	public FundTransferPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputPayerAcountNo(String payerAccountNo) {
		waitForElementVisible(driver, FundTransferPageUI.PAYER_ACCOUNT_NO_TXT);
		sendkeyToElement(driver, FundTransferPageUI.PAYER_ACCOUNT_NO_TXT, payerAccountNo);
	}
	
	public void inputPayeeAcountNo(String accountNo) {
		sendkeyToElement(driver, FundTransferPageUI.PAYEE_ACCOUNT_NO_TXT, accountNo);
	}

	public void inputAmount(String amount) {
		sendkeyToElement(driver, FundTransferPageUI.AMOUNT_TXT, amount);
	}
	
	public void inputDescription(String description) {
		sendkeyToElement(driver, FundTransferPageUI.DESCRIPTION_TXT, description);
	}
	
	public void clickSubmitButton() {
		clickToElement(driver, FundTransferPageUI.SUBMIT_BTN);
	}

	public String getDisplayMessage() {
		waitForElementVisible(driver, FundTransferPageUI.DISPLAY_MSG);
		return getTextElement(driver, FundTransferPageUI.DISPLAY_MSG).trim();
	}

	public String getAmount() {
		waitForElementVisible(driver, FundTransferPageUI.AMOUNT);
		return getTextElement(driver, FundTransferPageUI.AMOUNT).trim();
	}
	

}
