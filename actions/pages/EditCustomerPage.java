package pages;

import org.openqa.selenium.WebDriver;

import bankguru.EditCustomerPageUI;
import commons.AbstractPage;

public class EditCustomerPage extends AbstractPage {
	WebDriver driver;

	public EditCustomerPage(WebDriver _driver) {
		this.driver = _driver;
	}
	public void inputCustomerID(String customerID) {
		waitForElementVisible(driver, EditCustomerPageUI.CUSTOMERID_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.CUSTOMERID_TXT, customerID);
	}
	
	public void clickSubmitButton(){
		clickToElement(driver, EditCustomerPageUI.SUBMIT_BTN);
	}
	
	public void inputAddress(String address) {
		waitForElementVisible(driver, EditCustomerPageUI.ADDRESS_TXT);
		sendkeyToElement(driver, EditCustomerPageUI.ADDRESS_TXT, address);
	}

	public void inputCity(String city) {
		sendkeyToElement(driver, EditCustomerPageUI.CITY_TXT, city);
	}

	public void inputState(String state) {
		sendkeyToElement(driver, EditCustomerPageUI.STATE_TXT, state);
	}

	public void inputPinNumber(String pinNumber) {
		sendkeyToElement(driver, EditCustomerPageUI.PIN_TXT, pinNumber);
	}
	
	public void inputMobileNumber(String mobileNumber){
		sendkeyToElement(driver, EditCustomerPageUI.MOBILE_NUMBER_TXT, mobileNumber);
	}
	
	public void inputEmail(String email){
		sendkeyToElement(driver, EditCustomerPageUI.EMAIL_TXT, email);
	}	
	
	public String getSuccessfulMessage(){
		waitForElementVisible(driver, EditCustomerPageUI.SUCCESSFUL_MSG);
		return getTextElement(driver, EditCustomerPageUI.SUCCESSFUL_MSG).trim();
	}
}
