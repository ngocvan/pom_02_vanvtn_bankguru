package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import bankguru.NewCustomerPageUI;
import commons.AbstractPage;

public class NewCustomerPage extends AbstractPage {
	WebDriver driver;

	public NewCustomerPage(WebDriver _driver) {
		this.driver = _driver;
	}

	public void inputCustomerName(String customerName) {
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMER_NAME_TXT);
		sendkeyToElement(driver, NewCustomerPageUI.CUSTOMER_NAME_TXT, customerName);
	}

	public void inputDateOfBirth(String dateOfBirth) {
		sendkeyToElement(driver, NewCustomerPageUI.DATE_OF_BIRTH_TXT, dateOfBirth);
	}

	public void inputAddress(String address) {
		sendkeyToElement(driver, NewCustomerPageUI.ADDRESS_TXT, address);
	}

	public void inputCity(String city) {
		sendkeyToElement(driver, NewCustomerPageUI.CITY_TXT, city);
	}

	public void inputState(String state) {
		sendkeyToElement(driver, NewCustomerPageUI.STATE_TXT, state);
	}

	public void inputPinNumber(String pinNumber) {
		sendkeyToElement(driver, NewCustomerPageUI.PIN_TXT, pinNumber);
	}
	
	public void inputMobileNumber(String mobileNumber){
		sendkeyToElement(driver, NewCustomerPageUI.MOBILE_NUMBER_TXT, mobileNumber);
	}
	
	public void inputEmail(String email){
		sendkeyToElement(driver, NewCustomerPageUI.EMAIL_TXT, email);
	}
	
	public void inputPassword(String password){
		sendkeyToElement(driver, NewCustomerPageUI.PASSWORD_TXT, password);
	}
	
	public void clickSubmitButton(){
		clickToElement(driver, NewCustomerPageUI.SUBMIT_BTN);
	}
	
	public String getSuccessfulMessage(){
		waitForElementVisible(driver, NewCustomerPageUI.SUCCESSFUL_MSG);
		return getTextElement(driver, NewCustomerPageUI.SUCCESSFUL_MSG).trim();
	}
	
	public String getCustomerID(){
		waitForElementVisible(driver, NewCustomerPageUI.CUSTOMERID);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMERID).trim();
	}
	
	public void pressTabKey(String locator){
		waitForElementVisible(driver, locator);
		scrollToElement(driver, locator);
		clickToElement(driver, locator);
		dynamicKeyPress(driver, locator, Keys.TAB);
	}
	
	public String getErrorMessage(){
		waitForElementVisible(driver, NewCustomerPageUI.ERROR_LABEL);
		return getTextElement(driver, NewCustomerPageUI.ERROR_LABEL);
	}

}
