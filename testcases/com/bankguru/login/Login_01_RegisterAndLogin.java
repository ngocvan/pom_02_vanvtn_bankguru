package com.bankguru.login;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.PageFactoryManager;
import pages.DeleteCustomerPage;
import pages.EditCustomerPage;
import pages.HomePage;
import pages.LoginPage;
import pages.NewCustomerPage;
import pages.RegisterPage;

public class Login_01_RegisterAndLogin extends AbstractTest {

	public WebDriver driver;
	String email, username, password, loginURL;
	private LoginPage loginPage;
	private RegisterPage registerPage;
	private HomePage homePage;
	private NewCustomerPage newCustomerPage;
	private EditCustomerPage editCustomerPage;
	private DeleteCustomerPage deleteCustomerPage;

	@Parameters({ "browserName", "version" })
	@BeforeClass
	public void beforeTest(String browserName, String version) {
		log.info("==========Open Browser==========");
		driver = getMultiBrowser(browserName, version);
		loginPage = PageFactoryManager.getLoginPage(driver);
		email = "automation" + randomMailNumber() + "@gmail.com";
		log.info("Pre-condition step 01 -  get login URL");
		loginURL = loginPage.getURL();
	}

	@AfterClass
	public void afterTest() {
		log.info("========== close Browser ==========");
		closeBrowser();
	}

	@Test
	public void TC_01_RegisterToSystem() {
		log.info("Register step 01 -  click Here link");
		registerPage = loginPage.clickHereLink();
		log.info("Register step 02 -  input email");
		registerPage.inputEmail(email);
		log.info("Register step 03 -  click submit button");
		registerPage.clickSubmitButton();
		username = registerPage.getUsername();
		password = registerPage.getPassword();
	}

	@Test
	public void TC_02_Login() {
		log.info("Login step 01 -  open Login page");
		loginPage = registerPage.openLoginPage(loginURL);
		log.info("Register step 02 -  input username, password");
		loginPage.inputUsername(username);
		loginPage.inputPassword(password);
		log.info("Register step 02 -  click login button");
		homePage = loginPage.clickLoginButton();
		log.info("Register step 02 -  verify welcome message");
		verifyEquals("Welcome To Manager's Page of Guru99 Bank", homePage.getWelcomeHomepageMsg());
	}

	@Test
	public void TC_03_OpenDynamicPage() {
		log.info("TC_03_OpenDynamicPage step 01 -  open NewCustomerPage");
		newCustomerPage = homePage.openNewCustomerPage(driver);
		log.info("TC_03_OpenDynamicPage step 01 -  open DynamicPage");
		editCustomerPage = newCustomerPage.openEditCustomerPage(driver);
		log.info("TC_03_OpenDynamicPage step 01 -  open DynamicPage");
		deleteCustomerPage = editCustomerPage.openDeleteCustomerPage(driver);
	}

}
