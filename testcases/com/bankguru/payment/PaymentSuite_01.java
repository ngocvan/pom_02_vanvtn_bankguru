package com.bankguru.payment;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManager;
import pages.BalanceEnquiryPage;
import pages.DeleteAcountPage;
import pages.DeleteCustomerPage;
//import pages.DepositPage;
import pages.EditCustomerPage;
import pages.FundTransferPage;
import pages.HomePage;
import pages.LoginPage;
import pages.NewAccountPage;
import pages.NewCustomerPage;
import pages.TransactionPage;

public class PaymentSuite_01 extends AbstractTest {

	public WebDriver driver;
	private LoginPage loginPage;
	private HomePage homePage;
	private NewCustomerPage newCustomerPage;
	private EditCustomerPage editCustomerPage;
	private NewAccountPage newAccountPage;
	//private DepositPage depositPage;
	private TransactionPage transactionPage;	
	private FundTransferPage fundTransferPage;
	private BalanceEnquiryPage balanceEnquiryPage;
	private DeleteAcountPage deleteAcountPage;
	private DeleteCustomerPage deleteCustomerPage;
	String email, customerID, accountNo, payeeCountNo;
	int initialDeposit;

	@Parameters({ "browserName", "version" })
	@BeforeClass
	public void beforeTest(String browserName, String version) {
		log.info("========================================= Open Browser===================================");
		driver = getMultiBrowser(browserName, version);		
		loginPage = PageFactoryManager.getLoginPage(driver);
		email = "automation" + randomMailNumber() + "@gmail.com";
		initialDeposit = 50000;
	}
	
	@Test
	public void TC_01_CreateNewCustomer(){
		startTestCase("TC_01_CreateNewCustomer");
		log.info("TC_01_CreateNewCustomer - step 01 -  login");
		loginPage.inputUsername(Constants.USERNAME);
		loginPage.inputPassword(Constants.PWD);
		homePage = loginPage.clickLoginButton();
		verifyEquals("Welcome To Manager's Page of Guru99 Bank", homePage.getWelcomeHomepageMsg());
		log.info("TC_01_CreateNewCustomer - step 02 -  open New Customer Page");
		newCustomerPage = homePage.openNewCustomerPage(driver);
		log.info("TC_01_CreateNewCustomer - step 03 -  input information");
		newCustomerPage.inputCustomerName("AUTOMATION TESTING");
		newCustomerPage.inputDateOfBirth("01/01/1989");
		newCustomerPage.inputAddress("PO Box 911 8331 Duls Avenue");
		newCustomerPage.inputCity("Tampa");
		newCustomerPage.inputState("FL");
		newCustomerPage.inputPinNumber("466250");
		newCustomerPage.inputMobileNumber("4555442467");
		newCustomerPage.inputEmail(email);
		newCustomerPage.inputPassword("automation");
		log.info("TC_01_CreateNewCustomer - step 04 -  click submit button");
		newCustomerPage.clickSubmitButton();
		log.info("TC_01_CreateNewCustomer - step 05 -  verify successful message");
		verifyEquals("Customer Registered Successfully!!!", newCustomerPage.getSuccessfulMessage());
		customerID = newCustomerPage.getCustomerID();
		endTestCase();
	}
	
	@Test
	public void TC_02_EditCustomerAccount(){
		startTestCase("TC_02_EditCustomerAccount");
		log.info("TC_02_EditCustomerAccount - step 01 -  open Edit Customer Page");
		editCustomerPage = homePage.openEditCustomerPage(driver);
		log.info("TC_02_EditCustomerAccount - step 02 -  input customer id");
		editCustomerPage.inputCustomerID(customerID);
		log.info("TC_02_EditCustomerAccount - step 03 -  click submit button");
		editCustomerPage.clickSubmitButton();
		log.info("TC_02_EditCustomerAccount - step 04 -  update information");
		editCustomerPage.inputAddress("8331 Duls Avenue");
		editCustomerPage.inputCity("Houston");
		editCustomerPage.inputState("Texas");
		editCustomerPage.inputPinNumber("166455");
		editCustomerPage.inputMobileNumber("4777442467");
		editCustomerPage.inputEmail(email);
		log.info("TC_02_EditCustomerAccount - step 05 -  click submit button");
		newCustomerPage.clickSubmitButton();
		log.info("TC_02_EditCustomerAccount - step 06 -  verify edit successful message");
		verifyEquals("Customer details updated Successfully!!!", editCustomerPage.getSuccessfulMessage());
		endTestCase();
	}
	
	@Test
	public void TC_03_CreateNewAccount(){
		startTestCase("TC_03_CreateNewAccount");
		log.info("TC_03_CreateNewAccount - step 01 -  open NewAccount Page");
		newAccountPage = editCustomerPage.openNewAccountPage(driver);
		log.info("TC_03_CreateNewAccount - step 02 -  input customer id");
		newAccountPage.inputCustomerID(customerID);
		log.info("TC_03_CreateNewAccount - step 03 -  choose account type");
		newAccountPage.selectAccountType("Current");
		log.info("TC_03_CreateNewAccount - step 04 -  input Initial deposit	");
		newAccountPage.inputInitialDeposit(String.valueOf(initialDeposit));
		log.info("TC_03_CreateNewAccount - step 05 -  click submit button");
		newAccountPage.clickSubmitButton();
		log.info("TC_03_CreateNewAccount - step 06 -  verify successful message and current amount equal Initial deposit");
		verifyEquals("Account Generated Successfully!!!", newAccountPage.getSuccessfulMessage());
		verifyEquals(String.valueOf(initialDeposit), newAccountPage.getCurrentAmount());
		accountNo = newAccountPage.getAccountID();
		endTestCase();
	}
	
	@Test
	public void TC_04_TransferMoney(){
		startTestCase("TC_04_TransferMoney");
		log.info("TC_04_TransferMoney - step 01 -  open Deposit Page");
		transactionPage = newAccountPage.openDepositPage(driver);
		log.info("TC_04_TransferMoney - step 02 -  input account no");
		transactionPage.inputAcountNo(accountNo);;
		log.info("TC_04_TransferMoney - step 03 -  input amount");
		transactionPage.inputAmount("5000");
		log.info("TC_04_TransferMoney - step 04 -  input description");
		transactionPage.inputDescription("deposit");
		log.info("TC_04_TransferMoney - step 05 -  click submit button");
		transactionPage.clickSubmitButton();
		log.info("TC_04_TransferMoney - step 06 -  verify message display and current balance = 55000");
		verifyEquals("Transaction details of Deposit for Account " + accountNo, transactionPage.getDisplayMessage());
		verifyEquals("55000", transactionPage.getCurrentBalance());
		endTestCase();
	}
	
	@Test
	public void TC_05_WithdrawalMoney(){
		startTestCase("TC_05_WithdrawalMoney");
		log.info("TC_05_WithdrawalMoney - step 01 -  open Withdrawal Page");
		transactionPage = newAccountPage.openWithdrawalPage(driver);
		log.info("TC_05_WithdrawalMoney - step 02 -  input account no");
		transactionPage.inputAcountNo(accountNo);;
		log.info("TC_05_WithdrawalMoney - step 03 -  input amount");
		transactionPage.inputAmount("15000");
		log.info("TC_05_WithdrawalMoney - step 04 -  input description");
		transactionPage.inputDescription("deposit");
		log.info("TC_05_WithdrawalMoney - step 05 -  click submit button");
		transactionPage.clickSubmitButton();
		log.info("TC_05_WithdrawalMoney - step 06 -  verify message display and current balance = 40000");
		verifyEquals("Transaction details of Withdrawal for Account " + accountNo, transactionPage.getDisplayMessage());
		verifyEquals("40000", transactionPage.getCurrentBalance());
		endTestCase();
	}
	
	@Test
	public void TC_06_TransferMoneyToAnotherAccount(){
		startTestCase("TC_06_TransferMoney");
		log.info("TC_06_TransferMoney - step 01 -  create payee account");
		newAccountPage = transactionPage.openNewAccountPage(driver);
		newAccountPage.inputCustomerID(customerID);
		newAccountPage.selectAccountType("Current");
		newAccountPage.inputInitialDeposit(String.valueOf(initialDeposit));
		newAccountPage.clickSubmitButton();
		verifyEquals("Account Generated Successfully!!!", newAccountPage.getSuccessfulMessage());
		payeeCountNo = newAccountPage.getAccountID();
		
		log.info("TC_06_TransferMoney - step 02 -  open Fund Transfer page");
		fundTransferPage = newAccountPage.openFundTransferPage(driver);
		log.info("TC_06_TransferMoney - step 03 -  input payer account no");
		fundTransferPage.inputPayerAcountNo(accountNo);
		log.info("TC_06_TransferMoney - step 04 -  input payee account no");
		fundTransferPage.inputPayeeAcountNo(payeeCountNo);
		log.info("TC_06_TransferMoney - step 05 -  input amount");
		fundTransferPage.inputAmount("10000");
		log.info("TC_06_TransferMoney - step 06 -  input description");
		fundTransferPage.inputDescription("transfer");
		log.info("TC_06_TransferMoney - step 07 -  click submit button");
		fundTransferPage.clickSubmitButton();
		log.info("TC_06_TransferMoney - step 08 -  verify message: Fund Transfer Details display");
		verifyEquals("Fund Transfer Details", fundTransferPage.getDisplayMessage());
		log.info("TC_06_TransferMoney - step 09 -  verify Amount = 10000");
		verifyEquals("10000", fundTransferPage.getAmount());
		
		endTestCase();
	}
	
	@Test
	public void TC_07_BalanceEnquiry(){
		startTestCase("TC_07_BalanceEnquiry");
		log.info("TC_07_BalanceEnquiry - step 01 -  open Balance Enquiry Page");
		balanceEnquiryPage = fundTransferPage.openBalanceEnquiryPage(driver);
		log.info("TC_07_BalanceEnquiry - step 02 -  input account no");
		balanceEnquiryPage.inputAcountNo(accountNo);
		log.info("TC_07_BalanceEnquiry - step 05 -  click submit button");
		balanceEnquiryPage.clickSubmitButton();
		log.info("TC_07_BalanceEnquiry - step 06 - verify message: Balance Details for Account " + accountNo + " display");
		verifyEquals("Balance Details for Account " + accountNo, balanceEnquiryPage.getDisplayMessage());
		log.info("TC_07_BalanceEnquiry - step 06 - verify current balance");
		verifyEquals("30000", balanceEnquiryPage.getCurrentBalance());
		endTestCase();
	}
	
	@Test
	public void TC_08_DeleteAllAcount(){
		startTestCase("TC_08_DeleteAcount");
		log.info("TC_08_DeleteAcount - Delete payer account");
		log.info("TC_08_DeleteAcount - step 01 -  open Delete Account Page");
		deleteAcountPage = fundTransferPage.openDeleteAcountPage(driver);
		log.info("TC_08_DeleteAcount - step 02 -  input account no");
		deleteAcountPage.inputAcountNo(accountNo);
		log.info("TC_08_DeleteAcount - step 03 -  click submit button");
		deleteAcountPage.clickSubmitButton();
		log.info("TC_08_DeleteAcount - step 04 -  click ok button on Alert");
		deleteAcountPage.clickOKButtonOnAlert();
		verifyEquals("Account Deleted Sucessfully", deleteAcountPage.getTextOnAlert());
		deleteAcountPage.clickOKButtonOnAlert();		
		
		log.info("TC_08_DeleteAcount - Delete payee account");
		log.info("TC_08_DeleteAcount - step 01 -  open Delete Account Page");
		deleteAcountPage = fundTransferPage.openDeleteAcountPage(driver);
		log.info("TC_08_DeleteAcount - step 02 -  input account no");
		deleteAcountPage.inputAcountNo(payeeCountNo);
		log.info("TC_08_DeleteAcount - step 04 -  click submit button");
		deleteAcountPage.clickSubmitButton();
		log.info("TC_08_DeleteAcount - step 05 -  click ok button on Alert");
		deleteAcountPage.clickOKButtonOnAlert();
		verifyEquals("Account Deleted Sucessfully", deleteAcountPage.getTextOnAlert());
		deleteAcountPage.clickOKButtonOnAlert();
		
		endTestCase();
	}
	
	@Test
	public void TC_09_DeleteCustomer(){
		startTestCase("TC_08_DeleteAcount");
		log.info("TC_09_DeleteCustomert - step 01 -  open Delete Customer Page");
		deleteCustomerPage = deleteAcountPage.openDeleteCustomerPage(driver);
		log.info("TC_09_DeleteCustomer - step 02 -  input customer id");
		deleteCustomerPage.inputCustomerID(customerID);
		log.info("TC_09_DeleteCustomer - step 04 -  click submit button");
		deleteCustomerPage.clickSubmitButton();
		log.info("TC_09_DeleteCustomer - step 05 -  click ok button on Alert");
		deleteCustomerPage.clickOKButtonOnAlert();
		verifyEquals("Customer deleted Successfully", deleteCustomerPage.getTextOnAlert());
		deleteCustomerPage.clickOKButtonOnAlert();
		endTestCase();
	}

	@AfterClass
	public void afterTest() {
		log.info("=========================================== close Browser =====================================");
		closeBrowser();
	}
}
