package com.bankguru.customer;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bankguru.NewCustomerPageUI;
import commons.AbstractTest;
import commons.Constants;
import commons.PageFactoryManager;
import pages.HomePage;
import pages.LoginPage;
import pages.NewCustomerPage;

public class FielldValidationSuiteForAddNewCustomer extends AbstractTest{
	public WebDriver driver;
	private LoginPage loginPage;
	private NewCustomerPage newCustomerPage;
	private HomePage homePage;
	String email, customerID, accountNo, payeeCountNo;
	int initialDeposit;

	@Parameters({ "browserName", "version" })
	@BeforeClass
	public void beforeTest(String browserName, String version) {
		log.info("========================================= Open Browser===================================");
		driver = getMultiBrowser(browserName, version);		
		loginPage = PageFactoryManager.getLoginPage(driver);
		loginPage.inputUsername(Constants.USERNAME);
		loginPage.inputPassword(Constants.PWD);
		homePage = loginPage.clickLoginButton();
	}
	@AfterClass
	public void afterTest() {
		log.info("=========================================== close Browser =====================================");
		closeBrowser();
	}
	
	@Test
	public void TC_01_VerificationNameField(){
		startTestCase("TC_01_VerificationNameField");
		log.info("TC_01_VerificationNameField - step 01 -  Verify name cannot be empty");
		newCustomerPage = homePage.openNewCustomerPage(driver);
		newCustomerPage.pressTabKey(NewCustomerPageUI.CUSTOMER_NAME_TXT);
		verifyEquals("Customer name must not be blank", newCustomerPage.getErrorMessage());
		
		log.info("TC_01_VerificationNameField - step 02 -  Verify name cannot be numberic");
		newCustomerPage.inputCustomerName("name1234");
		verifyEquals("Numbers are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_01_VerificationNameField - step 03 -  Verify name cannot have special characters");
		newCustomerPage.inputCustomerName("name@!!@#");
		verifyEquals("Special characters are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_01_VerificationNameField - step 04 -  Verify name cannot have first character as blank space");
		newCustomerPage.inputCustomerName(" name");
		verifyEquals("First character can not have space", newCustomerPage.getErrorMessage());
	}
	
	@Test
	public void TC_02_VerificationAddressField(){
		startTestCase("TC_02_VerificationAddressField");
		log.info("TC_02_VerificationAddressField - step 01 -  Verify Address cannot be empty");
		newCustomerPage.inputCustomerName("Automation testing");
		newCustomerPage.inputDateOfBirth("01/01/1989");
		newCustomerPage.pressTabKey(NewCustomerPageUI.ADDRESS_TXT);
		verifyEquals("Address Field must not be blank", newCustomerPage.getErrorMessage());
		
		log.info("TC_02_VerificationAddressField - step 02 -  Verify Address cannot have first character as blank space");
		newCustomerPage.inputAddress(" address");
		verifyEquals("First character can not have space", newCustomerPage.getErrorMessage());
	}
	
	@Test
	public void TC_03_VerificationCityField(){
		startTestCase("TC_03_VerificationCityField");
		log.info("TC_03_VerificationCityField - step 01 -  Verify City cannot be empty");
		newCustomerPage.inputAddress("PO Box 911 8331 Duls Avenue");
		newCustomerPage.pressTabKey(NewCustomerPageUI.CITY_TXT);
		verifyEquals("City Field must not be blank", newCustomerPage.getErrorMessage());
		
		log.info("TC_03_VerificationCityField - step 02 -  Verify City cannot be numberic");
		newCustomerPage.inputCity("123");
		verifyEquals("Numbers are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_03_VerificationCityField - step 03 -  Verify City cannot have special characters");
		newCustomerPage.inputCity("city@!!@#");
		verifyEquals("Special characters are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_03_VerificationCityField - step 04 -  Verify City cannot have first character as blank space");
		newCustomerPage.inputCity(" city");
		verifyEquals("First character can not have space", newCustomerPage.getErrorMessage());
	}
	
	@Test
	public void TC_04_VerificationStateField(){
		startTestCase("TC_04_VerificationStateField");
		log.info("TC_04_VerificationStateField - step 01 -  Verify State cannot be empty");
		newCustomerPage.inputCity("texas");
		newCustomerPage.pressTabKey(NewCustomerPageUI.STATE_TXT);
		verifyEquals("State must not be blank", newCustomerPage.getErrorMessage());
		
		log.info("TC_04_VerificationStateField - step 02 -  Verify State cannot be numberic");
		newCustomerPage.inputState("123");
		verifyEquals("Numbers are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_04_VerificationStateField - step 03 -  Verify State cannot have special characters");
		newCustomerPage.inputState("state @!!@#");
		verifyEquals("Special characters are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_04_VerificationStateField - step 04 -  Verify State cannot have first character as blank space");
		newCustomerPage.inputState(" state");
		verifyEquals("First character can not have space", newCustomerPage.getErrorMessage());
	}
	
	@Test
	public void TC_05_VerificationPinField(){
		startTestCase("TC_05_VerificationPinField");
		log.info("TC_05_VerificationPinField - step 01 -  Verify PIN cannot be empty");
		newCustomerPage.inputState("PL");
		newCustomerPage.pressTabKey(NewCustomerPageUI.PIN_TXT);
		verifyEquals("PIN Code must not be blank", newCustomerPage.getErrorMessage());
		
		log.info("TC_05_VerificationPinField - step 02 -  Verify PIN must be numberic");
		newCustomerPage.inputPinNumber("abc");
		verifyEquals("Characters are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_05_VerificationPinField - step 03 -  Verify PIN must have6 digits");
		newCustomerPage.inputPinNumber("123");
		verifyEquals("PIN Code must have 6 Digits", newCustomerPage.getErrorMessage());
		
		log.info("TC_05_VerificationPinField - step 04 -  Verify PIN cannot have special characters");
		newCustomerPage.inputPinNumber("123@!!@#");
		verifyEquals("Special characters are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_05_VerificationPinField - step 05 -  Verify PIN cannot have first character as blank space");
		newCustomerPage.inputPinNumber(" 12345");
		verifyEquals("First character can not have space", newCustomerPage.getErrorMessage());
		
		log.info("TC_05_VerificationPinField - step 06 -  Verify PIN cannot have blank space");
		newCustomerPage.inputPinNumber("123 45");
		verifyEquals("Characters are not allowed", newCustomerPage.getErrorMessage());
	}
	
	@Test
	public void TC_06_VerificationTelephoneField(){
		startTestCase("TC_06_VerificationTelephoneField");
		log.info("TC_06_VerificationTelephoneField - step 01 -  Verify Telephone cannot be empty");
		newCustomerPage.inputPinNumber("123456");
		newCustomerPage.pressTabKey(NewCustomerPageUI.MOBILE_NUMBER_TXT);
		verifyEquals("Mobile no must not be blank", newCustomerPage.getErrorMessage());
		
		log.info("TC_06_VerificationTelephoneField - step 02 -  Verify Telephone cannot have first character as blank space");
		newCustomerPage.inputMobileNumber(" 12345");
		verifyEquals("First character can not have space", newCustomerPage.getErrorMessage());
		
		log.info("TC_06_VerificationTelephoneField - step 03 -  Verify Telephone cannot have blank space");
		newCustomerPage.inputMobileNumber("0123 34345");
		verifyEquals("Characters are not allowed", newCustomerPage.getErrorMessage());
		
		log.info("TC_06_VerificationTelephoneField - step 04 -  Verify Telephone cannot have special characters");
		newCustomerPage.inputMobileNumber("012389@!!@#");
		verifyEquals("Special characters are not allowed", newCustomerPage.getErrorMessage());
	}

}
